﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Components;

namespace QuackHack
{
    static class Program
    {

        [DllImport("FreeImage.dll", EntryPoint = "FreeImage_Initialise")]
        public static extern void Initialize(bool loadLocalPluginsOnly);


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain());
            Initialize(true);
        }

        static void doNothing()
        {
            new MetroToolTip();
            new StrapUp.Forms.Controls.CardView();
        }
        
    }
}
