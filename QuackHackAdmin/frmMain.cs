﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using log4net;

namespace QuackHack
{
    public partial class frmMain : Form
    {
        private static ILog log = LogManager.GetLogger("Quackathon");

        public frmMain()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            log.Info("We're running!");
        }
    }
}
